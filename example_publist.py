from __future__ import print_function, division
from publistmaker import publistMaker
import codecs
import sys

my_supervisors = ('Wheeler', 'Milosavljević', 'Lamb')
my_orcid = '0000-0002-5080-5996'
p = publistMaker(orcid=my_orcid,max_pages=5)

beg_list = r'\begin{etaremune}' + '\n\n'
end_list = r'\end{etaremune}' + '\n\n'

def progress(stem,fraction_done):
    print('{0}: {1:2.0%}'.format(stem,fraction_done),end='\r')
    sys.stdout.flush()

if False:
    cnt=0
    with codecs.open('formD.tex',encoding='utf-8',mode='w') as f:
        f.write(beg_list)
        for paper in p.FormD(supervisors=my_supervisors,my_name='Couch, S'):
            f.write(paper)
            cnt += 1
            progress('writing formD.tex',cnt/len(p.refereed))
        f.write(end_list)
    print("\n")

cnt=0
with codecs.open('refereed-publication-list.tex',encoding='utf-8',mode='w') as f:
    f.write(beg_list)
    for paper in p.CV(pubtype='allarticles'):
        f.write(paper)
        cnt += 1
        progress('writing refereed-publication-list.tex',cnt/len(p.refereed))
    f.write(end_list)
print("\n")

cnt=0
with codecs.open('contributed-list.tex',encoding='utf-8',mode='w') as f:
    f.write(beg_list)
    for paper in p.CV(pubtype='contributed'):
        f.write(paper)
        cnt += 1
        progress('writing contributed-list.tex',cnt/len(p.contributed))
    f.write(end_list)
print("\n")

chart = p.histogram_maker()
table_preamble = r'''\begin{longtable}[c]{lrrrrrrrrrrrrr}
\caption{Citation record, in reverse chronological order, of peer-reviewed articles.\label{t.citation-history}}\\
paper & 05 & 06 & 07 & 08 & 09 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & total\\
\hline\endfirsthead
paper & 05 & 06 & 07 & 08 & 09 & 10 & 11 & 12 & 13 & 14 & 15 & 16 & total\\
\hline\endhead
'''

print('writing citation-table.tex')
with open('citation-table.tex',mode='w') as f:
    f.write(table_preamble)
    for row in chart:
        f.write(row)
    f.write(r'\end{longtable}' + '\n\n')
