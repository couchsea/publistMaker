# useful formating functions
def safe_format(func):
    def inner(s):
        try:
            return func(s)
        except TypeError:
            return s
    return inner

@safe_format
def boldface(s):
    return r'{\bfseries '+s+'}'

@safe_format
def italic(s):
    return r'{\itshape '+s+'}'

@safe_format
def underline(s):
    return r'\underline{'+s+'}'

@safe_format
def quoted(s):
    return '``'+s+"''"

def format_author(author):
    """
    converts author name in form 'surname, name1 name2 ...' to 
    'surname, n. n. ...'
    """
    surname,sep,firstnames = author.partition(',')
    initials = '. '.join(s[0] if not s.startswith('-') else s[0:2] for s in firstnames.split()).replace('. -','.-') + '.'
    return ', '.join((surname,initials))

def texify(s):
    """
    replaces html superscripts and subscripts with tex ones. TeX-escapes 
    ampersands.
    """
    return s.replace('<SUP>','$^{').replace('</SUP>','}$').replace('<SUB>','$_{').replace('</SUB>','}$').replace('&','\&').replace('#','\#')

def CV_format_entry(paper,max_authors=8,etal=3,list_cites=True,my_name='nobody'):
    """
    Formats an ADS article entry into a format for a CV: quoted title (which is 
    hyperlinked to ADS page), followed by a list of authors.  If the number of 
    authors exceeds a set limit, the list is truncated to 'et al.'.  After the 
    author list follows the year, journal, volume, and page. The number of 
    citations to the article is also reportable.
    
    Parameters
    ----------
    paper : ads.Article
        entry, as returned by ads.SearchQuery
    max_authors : int, optional
        Maximum number of authors in list [default 8]
    etal : int, optional
        If author list truncated, number of authors listed before "et al."
        [default 3]
    list_cites : boolean, optional
        if True [default] list the number of citations following the page.
    my_name : str, optional
        String used for tagging your papers, e.g. 'Brown, E'.  Note that this
        string must be a substring of the listing in surname, initial format. 

    Returns
    -------
    entry : string
        tex-formatted listing for article
    """
    truncate = len(paper.author) > max_authors
    authors = paper.author if not truncate else paper.author[0:etal]
    authors = [ format_author(author) for author in authors ]
    authors = [ boldface(author) if my_name in author else author for author in authors]
    authorlist = ', '.join(author for author in authors)
    if truncate:
        authorlist += ', et al.\\'

    url=u'https://ui.adsabs.harvard.edu/#abs/'+paper.bibcode+'/abstract'
    title = ''.join((r'\href{',url,'}{',quoted(texify(paper.title[0])),'}'))
    entry = r'\item '+title+', '+authorlist+' '
    page = paper.page[0] if paper.page else None
    pub = texify(paper.pub) if paper.pub else None
    entry += ', '.join(s for s in (paper.year,italic(pub),paper.volume,page) if s)
    if list_cites:
        entry += ' ({} citations)'.format(paper.citation_count)
    entry += '\n\n'
    return entry

def FormD_format_entry(paper,supervisors={},my_name='nobody',underline_first=True):
    """
    Formats an ADS article entry into MSU's dread FormD format.
    
    Form D formatting rules:
        1. refereed entries preceded by asterisk
        2. title is in italics if work done with thesis or postdoctoral 
            supervisor(s)
        3. title is in boldface if work done at MSU
        4. the lead author (first author for astronomy) is underlined. There
            is no way to automatically do this, since that information is not
            reported.
    
    Parameters
    ----------
    paper : ads.Article
        entry, as returned by ads.SearchQuery
    supervisors : set(str), optional
        set of strings holding last names of supervisors, e.g., 
        {'Bildsten','Truran','Rosner','Lamb'}
    my_name : str, optional
        String used for tagging your papers, e.g. 'Brown, E'.  Note that this
        string must be a substring of the listing in surname, initial format. 
    underline_first : boolean, optional
        If true, presume the first author is the lead, as conventional in
        astronomy.  If false, there is no underlining, as one cannot determine
        the lead author from the information in the bibliographic information.
    
    Returns
    -------
    entry : string
        tex-format FormD listing for article
    """

    # put authors into lastname, initial format
    authors = [ format_author(author) for author in paper.author ]

    # flag if paper was done with supervisor(s) or at MSU
    surnames = {author.partition(',')[0] for author in paper.author}
    with_supervisor = not surnames.isdisjoint(supervisors)
    at_MSU = any(my_name in author and 'Michigan' in aff for author, aff in zip(authors,paper.aff))

    # Format author list
    # boldface me
    authors = [ boldface(author) if my_name in author else author for author in authors]
    
    # underline first author if so desired
    if underline_first:
        authors[0] = underline(authors[0])
    
    # compose author list
    authorlist = ', '.join(authors)

    # Format title per rules 2 and 3
    title = quoted(texify(paper.title[0]))
    if at_MSU:
        title = boldface(title)
    if with_supervisor:
        title = italic(title)

    # assemble entry: title has following newline
    entry = r'\item '
    # Rule 1 
    if 'REFEREED' in paper.property:
        entry += r'\textsuperscript{*}'
    entry += title+r'\\'+'\n'
    # authors
    entry += authorlist+' '
    page = paper.page[0] if paper.page else None
    pub = texify(paper.pub) if paper.pub else None
    entry += ', '.join(s for s in (paper.year,italic(pub),paper.volume,page) if s)
    # append newline for readability in tex source
    entry += '\n\n'
    return entry
