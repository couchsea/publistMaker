from __future__ import division, print_function
from time import localtime
import numpy as np
import ads
from .formatting import CV_format_entry, FormD_format_entry

class publistMaker:
    """
    wrapper class to assemble tex-formatted publication and citation records
    """

    def __init__(self,**kwargs):
        """
        constructor

        Parameters
        ----------
        **kwargs : optional
            flags to pass to ads.SearchQuery.  At present these are just basic
            parameters like name, orcid, date...
        """

        if 'sort' not in kwargs:
            kwargs['sort'] = 'date'
        print('retrieving records from ads with search: '+', '.join('{0} = {1}'.format(k,v) for k, v in kwargs.items()))
        filters = {
            'allarticles':'property:article',
            'refereed':'property:refereed AND property:article',
            'unrefereed':'property:notrefereed AND property:article',
            'contributed':'property:notrefereed AND property:nonarticle'
        }
        req_fields = [ 'id','author','bibcode','title','page','pub','year',
        'volume','citation_count','property','first_author','aff' ]
        self.refereed = list(ads.SearchQuery(fq=filters['refereed'],fl=req_fields,**kwargs))
        print('got {} refereed'.format(len(self.refereed)))
        self.unrefereed = list(ads.SearchQuery(fq=filters['unrefereed'],fl=req_fields,**kwargs))
        print('got {} unrefereed'.format(len(self.unrefereed)))
        self.contributed = list(ads.SearchQuery(fq=filters['contributed'],fl=req_fields,**kwargs))
        print('got {} contributed'.format(len(self.contributed)))
        self.allarticles = list(ads.SearchQuery(fq=filters['allarticles'],fl=req_fields,**kwargs))
        print('got {} allarticles'.format(len(self.allarticles)))
        self.have_metrics=False

    def FormD(self,pubtype='refereed',supervisors='',my_name='nobody',underline_first=True):
        """
        Returns generator for iterating over FormD publication list of refereed
        entries.

        Parameters
        ----------
        pubtype : str, optional
            type of list.  Options are 'refereed', 'unrefereed', and
            'contributed'
        supervisors : set(str), optional
            set of strings holding last names of supervisors, e.g.,
            {'Bildsten','Truran','Rosner','Lamb'}
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        underline_first : boolean, optional
            If true, presume the first author is the lead, as conventional in
            astronomy. If false, there is no underlining, as one cannot
            determine the lead author from the information in the bibliographic
            information.
        """

        if pubtype == 'refereed':
            l = self.refereed
        elif pubtype == 'unrefereed':
            l = self.unrefereed
        elif pubtype == 'contributed':
            l = self.contributed
        else:
            print('unknown pubtype {}'.format(pubtype))
            return None

        return ( FormD_format_entry(entry,supervisors=supervisors,my_name=my_name,underline_first=underline_first) for entry in l )

    def CV(self,pubtype='refereed',max_authors=8,etal=3,list_cites=True,my_name='nobody'):
        """
        Returns generator for iterating over tex-format listing of publications.

        Parameters
        ----------
        pubtype : str, optional
            type of list.  Options are 'refereed', 'unrefereed', and
            'contributed'
        max_authors : int, optional
            Maximum number of authors in list [default 8]
        etal : int, optional
            If author list truncated, number of authors listed before "et al."
            [default 3]
        list_cites : boolean, optional
            if True [default] list the number of citations following the page.
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        """

        if pubtype == 'refereed':
            l = self.refereed
        elif pubtype == 'unrefereed':
            l = self.unrefereed
        elif pubtype == 'contributed':
            l = self.contributed
        elif pubtype == 'allarticles':
            l = self.allarticles
        else:
            print('unknown pubtype {}'.format(pubtype))
            return None
        #print(l)
        return ( CV_format_entry(entry,max_authors=max_authors,etal=etal,list_cites=list_cites,my_name=my_name) for entry in l)

    def get_metrics(self,number_years=12,end_year='now',my_name='nobody'):
        """
        Compiles publication and citation metrics for refereed publications

        Parameters
        ----------
        number_years : int, optional
            Span of years for which citations by year and publications per year
            are reported
        end_year : str, optional
            if 'now' the current year is used.  Otherwise, one may supply a
            value, e.g. '2004'.
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        """

        # set the range of years over which to generate the histogram.
        end_year = localtime().tm_year if end_year == 'now' else int(end_year)
        first_year=end_year-number_years+1
        self.citations_per_article = np.zeros(shape=(len(self.refereed),number_years),dtype=np.int)
        self.first_author_pubs = np.zeros(number_years,dtype=np.int)
        self.coauthor_pubs = np.zeros(number_years,dtype=np.int)
        self.first_author_cites = np.zeros(number_years,dtype=np.int)
        self.coauthor_cites = np.zeros(number_years,dtype=np.int)
        self.metrics = { 'first year':first_year, 'end year':end_year }

        years = [ str(y) for y in range(first_year,first_year+number_years) ]

        for paper_id,paper in enumerate(self.refereed):

            this_paper_year_id = int(paper.year)-first_year
            first_author = my_name in paper.first_author

            if this_paper_year_id >= 0 and this_paper_year_id < number_years:
                if first_author:
                    self.first_author_pubs[this_paper_year_id] += 1
                else:
                    self.coauthor_pubs[this_paper_year_id] += 1

            try:
                for y,c in paper.metrics['histograms']['citations']['refereed to refereed'].items():
                    year_id=int(y)-first_year
                    if year_id >= 0 and year_id < number_years:
                        self.citations_per_article[paper_id,year_id] += c
            except KeyError:
                print('no refereed metrics available for paper {0}: {1}'.format(paper_id,paper.title))

            try:
                for y, c in paper.metrics['histograms']['citations']['nonrefereed to refereed'].items():
                    year_id = int(y)-first_year
                    if year_id >= 0 and year_id < number_years:
                        self.citations_per_article[paper_id,year_id] += c

            except KeyError:
                print('no nonrefereed metrics available for paper {0}: {1}'.format(paper_id,paper.title))

            if first_author:
                self.first_author_cites[:] += self.citations_per_article[paper_id,:]
            else:
                self.coauthor_cites[:] += self.citations_per_article[paper_id,:]

        self.have_metrics=True

    def histogram_maker(self,suppress_zero=True,generate_metrics=False,**kwargs):
        """
        Makes a histogram of citations per article per year in tex format,
        suitable for pasting into a `tabular` or `longtable` format.

        Parameters
        ----------
        suppress_zero : boolean, optional
            if True (default), a blank column is inserted for those years prior
            to publication for which there are no citations.
        generate_metrics : boolean, optional
            if True, recalculate the publication metrics.
        **kwargs : optional
            parameters passed to `get_metrics`

        Returns
        -------
        citation_table : list(str)
            each item in the list is one row of the tex-format histogram
        """

        if not self.have_metrics or generate_metrics:
            print('generating statistics')
            self.get_metrics(**kwargs)

        citation_table = []
        years = [ str(y) for y in range(self.metrics['first year'],self.metrics['end year']+1) ]
        Ncols = len(years)+2
        current_year = ''
        paper_id = len(self.refereed)
        for article,paper in zip(self.citations_per_article,self.refereed):
            row = ''
            # if a new year, write out a banner
            if paper.year != current_year:
                row = r'\multicolumn{{{0}}}{{c}}{{{1}}}\\'.format(Ncols,paper.year)+'\n'
                current_year = paper.year

            # write out table: publication id, histogram
            row += '{}'.format(paper_id)

            for cites,year in zip(article,years):
                row += ' &' if suppress_zero and cites == 0 and int(paper.year) > int(year) else ' & {}'.format(cites)

            row += r' & {} \\'.format(article.sum())+'\n'
            citation_table.append(row)
            paper_id -= 1
        return citation_table
