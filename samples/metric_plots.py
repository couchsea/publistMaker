# example of a script to make a nice plot of publications, citations

from __future__ import print_function, division
from publistmaker import publistMaker
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.colors import hsv_to_rgb
import numpy as np
from datetime import datetime

# get the papers
my_supervisors = ('Bildsten','Truran','Lamb','Rosner')
my_orcid = '0000-0003-3806-5339'
p = publistMaker(orcid=my_orcid,sort='citation_count')

# make the histograms
p.get_metrics()

tend = datetime.now().year
nyears = 12
tstart = tend-nyears+1
this_year = -1
last_year = -2

allpubs = p.first_author_pubs + p.coauthor_pubs
allcites = p.first_author_cites + p.coauthor_cites

#set the preprints
preprints = np.zeros_like(allpubs)
preprints[last_year] += 1
preprints[this_year] += 1

# plots have ivory background, greenish-blue bars, red bar for preprints
ivory = hsv_to_rgb((47/360,0.12,1.00))
first_clr = hsv_to_rgb((150/360,0.24,0.66))
preprint_clr = hsv_to_rgb((14/360,1.00,0.85))
charsize=10
major_ticklength=0.6*charsize
major_tickwidth=0.9
minor_ticklength=0.3*charsize
minor_tickwidth=0.7
rc('figure',**{'figsize':(5,3)})
rc('mathtext',**{'fontset':'stixsans'})
rc('font',**{'size':charsize,'sans-serif':'Bitstream Vera Sans'})
rc('axes',**{'titlesize':charsize,'labelsize':charsize,'facecolor':ivory})
rc('xtick',**{'major.size':major_ticklength,'major.width':major_tickwidth,'labelsize':charsize,'direction':'out'})
rc('ytick',**{'major.size':major_ticklength,'major.width':major_tickwidth,'labelsize':charsize,'direction':'out'})

plt.xlim(tstart-0.5,tend+0.5)
plt.ylim(0,allpubs.max()*1.05)
plt.xlabel('year')
plt.ylabel('publications')
plt.bar(range(tstart,tend+1),preprints,align='center',bottom=allpubs,color=preprint_clr,label='preprints')
plt.bar(range(tstart,tend+1),allpubs,align='center',color=first_clr,label='refereed')
plt.legend(loc='upper left',frameon=False,fontsize='small')
plt.tight_layout()
plt.savefig('my-publications.pdf',format='pdf')
plt.clf()

plt.xlim(tstart-0.5,tend+0.5)
plt.ylim(0,allcites.max()*1.05)
plt.xlabel('year')
plt.ylabel('citations')
plt.bar(range(tstart,tend+1),allcites,align='center',color=first_clr)
plt.tight_layout()
plt.savefig('my-citations.pdf',format='pdf')
