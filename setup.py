from setuptools import setup

setup(
    name='publistmaker',
    version='0.1.3',
    description='class to pull citation statistics from ADS and format them into reports',
    url='https://gitlab.msu.edu/browned/publistMaker',
    author='Edward Brown',
    author_email='ebrown@pa.msu.edu',
    license='MIT',
    packages=['publistmaker'],
	install_requires=['ads',],
    zip_safe=False
)
